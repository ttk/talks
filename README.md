# SPAM talks
[![build artifacts](https://img.shields.io/gitlab/pipeline-status/ttk/talks?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&label=Compilation&logo=latex&style=for-the-badge)](https://gricad-gitlab.univ-grenoble-alpes.fr/ttk/talks/-/pipelines)
[![Downloads](https://img.shields.io/gitlab/v/tag/ttk/talks?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&label=Downloads&sort=date&style=for-the-badge)](https://gricad-gitlab.univ-grenoble-alpes.fr/ttk/talks/-/jobs/artifacts/main/browse?job=pdf)

Some of our talks about SPAM:
- Journées du laboratoire 3SR (2022/05/30)

[Download the slides](https://gricad-gitlab.univ-grenoble-alpes.fr/ttk/talks/-/jobs/artifacts/main/browse?job=pdf)
